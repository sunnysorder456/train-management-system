from django.http import JsonResponse
from django.shortcuts import render
from pathlib import Path
import os
from ttms.form import InfoForm, ImageForm
from ttms.models import Person, Student

from .main import main
# Create your views here.
def get_info(request):
    context = {}
    if request.method == "POST":
        form = InfoForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
    else:
        form = InfoForm()
    context['form'] = form

    return render(request, 'info.html', context=context)


def get_all_persons(request):
    persons = Person.objects.all()
    context = {'persons': persons}
    return render(request, 'persons.html', context=context)


def scan_image(request):
    if request.method == "POST":
        print(request.FILES, request.POST)
        # form = ImageForm(request.POST, request.FILES)
        # if form.is_valid():
        return JsonResponse({"instance": 'save'}, status=200)
        # else:
        #     return JsonResponse({"error": form.errors}, status=400)

    return render(request, 'scan_image.html')


def home(request):
    BASE_DIR = Path(__file__).resolve().parent.parent


    img_path = os.path.join(BASE_DIR, "{}/{}".format("media", "img.jpg"))

    info = main(img_path)

    # print(info["roll"])
    qs = Student.objects.filter(roll=info["roll"]).values()

    if qs.count() > 0:
        check = 1
    else:
        check = 0


    # print(check)

    context = {
        'name': info["student_name"],
        'roll': info["roll"],
        'dept': info["dept"],
        'check': check,
    }


    return render(request, 'home.html', context)