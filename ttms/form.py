from django import forms

from ttms.models import Person


class DateInput(forms.DateInput):
    input_type = 'date'


class InfoForm(forms.ModelForm):
    # first_name = forms.CharField()
    # last_name = forms.CharField()
    # n_id = forms.CharField()
    # image = forms.ImageField()
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs[
                'class'] = "block p-2 w-full text-gray-900 bg-gray-50 rounded-sm border border-gray-300 sm:text-xs focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            visible.field.widget.attrs['placeholder'] = visible.field.label

    class Meta:
        model = Person
        fields = '__all__'

        widgets = {
            'dob': DateInput()
        }


class ImageForm(forms.Form):
    scan_image = forms.ImageField()
