#import keras_ocr

#import pytesseract


# keras_ocr
# pipeline = keras_ocr.pipeline.Pipeline()
# def imageRead(img):
#     prediction = pipeline.recognize(img)
#     return prediction

# easyocr
import easyocr
def imageRead(frame):
	reader = easyocr.Reader(['en'])
	result = reader.readtext(frame)
	# print(result[1:][1])
	# print(result[1][1], result[2][1], result[3][1])
	return result

	# for i in result:
	# 	print(i)

# pytesseract
# pytesseract.pytesseract.tesseract_cmd=r"C:\Program Files\Tesseract-OCR\tesseract.exe"
# def imageRead(img):
#     text = pytesseract.image_to_string(img)
#     return text