from django.db import models

from djangoProject.mixins.models import TimeStampMixin


# Create your models here.
class Person(TimeStampMixin):
    first_name = models.CharField(max_length=30, null=True, blank=True)
    last_name = models.CharField(max_length=30, null=True, blank=True)
    n_id = models.CharField(max_length=30, null=False, blank=False)
    dob = models.DateField(null=False, blank=False)
    image = models.ImageField(upload_to='images/', null=False, blank=False)


class Student(models.Model):
    name = models.CharField(max_length=255)
    institute = models.CharField(max_length=255)
    roll = models.CharField(max_length=255)
    department = models.CharField(max_length=255)
    blood_group = models.CharField(max_length=255)
    valid_till = models.CharField(max_length=255)

    def __str__(self):
        return self.name+" "+self.roll






