def pt(text):

    # To print out the entire text fields in the given text array
    # for i in range(len(text)):
    #     print(text[i][1])

    # To process text fields in student ID card
    if text[0][1] == "STUDENT":
        pt.institute = text[1][1] + " " + text[2][1]
        pt.student_name = text[3][1] + text[4][1] + text[5][1]
        pt.blood = text[6][1][14:]
        pt.roll = text[8][1]
        pt.dept = text[9][1]
        pt.valid_till = text[10][1][11:]
