import time
import cv2
import imutils as imutils
from matplotlib import pyplot as plt
import numpy as np
from .readText import imageRead
from .processText import pt

def blurDetection(image):
	# compute the Laplacian of the image and then return the focus
	# measure, which is simply the variance of the Laplacian
	return cv2.Laplacian(image, cv2.CV_64F).var()

notBlur = True
threshold = 500

vid = cv2.VideoCapture(2)
def main(img_path):
	count = 0

	while notBlur:

		# capture video frame by frame
		# ret, frame = vid.read()

		# single image for test purpose
		frame = cv2.imread(img_path)
		frame = imutils.resize(frame, width=800)

		# cv2.imshow('frame', frame)

		# time.sleep(3)
		fm = blurDetection(frame)

		values = dict()
		if fm > threshold and count < 1:
			# notBlur = False
			print("unblurred image captured")
			# ReadText(frame)
			output = imageRead(frame)
			pt(output)

			# print("student name:" + pt.student_name)
			# print("roll: " + pt.roll)
			# print("blood: " + pt.blood)
			# print("department: " + pt.dept)
			# print("valid_till: " + pt.valid_till)
			# print("institute: " + pt.institute)

			values = {
				'student_name': pt.student_name,
				'roll': pt.roll,
				'blood': pt.blood,
				'dept': pt.dept,
				'valid_till': pt.valid_till,
				'institute': pt.institute,

			}
			# print(output)
			count += 1

			# print(values)

			return values

		# 'q' button set for quitting the video
		if cv2.waitKey(1) & 0xFF == ord('q'):
			break

