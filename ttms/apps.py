from django.apps import AppConfig


class TtmsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ttms'
