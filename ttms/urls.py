from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('persons', views.get_all_persons, name='persons'),
    path('scan', views.scan_image, name='scan'),
]
